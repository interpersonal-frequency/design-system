// Documentation on theming Storybook: https://storybook.js.org/docs/configurations/theming/

import { create } from '@storybook/theming';

export default create({
  base: 'light',
  // Branding
  brandTitle: 'IF Design System',
  brandUrl: 'https://ifsight.com',
  brandImage:
    'https://bitbucket.org/interpersonal-frequency/design-system/raw/cc65b8aed38e6ccca907c71e28eb7665b39c9f19/images/CF_Main_Color_logo_3x.png',
});
