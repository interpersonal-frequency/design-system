module.exports = {
  tagFormat: '${version}',
  branches: ['main'],
  repositoryUrl: 'git@bitbucket.org:interpersonal-frequency/design-system.git',
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/github',
  ],
};
