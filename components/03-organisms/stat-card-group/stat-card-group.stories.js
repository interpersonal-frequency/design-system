import statCardGroupTemplate from './stat-card-group.twig';
import statCardGroupData from './stat-card-group.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    columns: {
      options: [2, 3, 4],
      control: { type: 'select' },
    },
    stat_card_group: {
      control: { type: 'array' },
    },
    section_title: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return statCardGroupTemplate({ label, ...args });
};

export const StatCardGroup = Template.bind({});
StatCardGroup.args = {
  columns: 4,
  section_title: 'Your Support in Action',
  stat_card_group: statCardGroupData.items,
};
