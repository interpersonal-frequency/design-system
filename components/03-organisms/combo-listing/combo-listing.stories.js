import comboListing from './combo-listing.twig';
import cardListingData from '../card-listing/card-listing.yml';
import eventsData from '../event-listing/event-listing.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    card: { control: 'object' },
    events: { control: 'object' },
  },
};

const Template = ({ label, ...args }) => {
  return comboListing({ label, ...args });
};

export const ComboListing = Template.bind({});
ComboListing.args = {
  card: cardListingData,
  events: eventsData.events,
};
