import eventCardGroupTemplate from './event-card-group.twig';

import eventCardGroupData from './event-card-group.yml';

/**
 * Storybook Definition.
 */

export default {
  title: 'Organisms',
  argTypes: {
    columns: {
      options: [2, 3],
      control: { type: 'select' },
    },
    border: {
      control: 'select',
      options: ['top', 'bottom', 'none'],
    },
    event_cards: {
      control: { type: 'array' },
    },
  },
};

const Template = ({ label, ...args }) => {
  return eventCardGroupTemplate({ label, ...args });
};

export const eventCardGroup = Template.bind({});
eventCardGroup.args = {
  columns: 2,
  border: 'top',
  event_cards: eventCardGroupData.event_card_items,
  img__src: 'https://picsum.photos/500/400',
  img__alt: 'Image alt text',
};
