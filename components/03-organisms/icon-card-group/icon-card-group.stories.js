import iconCardGroupTemplate from './icon-card-group.twig';
import iconCardGroupData from './icon-card-group.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    columns: {
      options: [2, 3, 4],
      control: { type: 'select' },
    },
    icon_cards: {
      control: { type: 'array' },
    },
  },
};

const Template = ({ label, ...args }) => {
  return iconCardGroupTemplate({ label, ...args });
};

export const iconCardGroup = Template.bind({});
iconCardGroup.args = {
  columns: 4,
  icon_cards: iconCardGroupData.items,
};
