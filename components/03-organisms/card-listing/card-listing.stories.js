import cardListingTemplate from './card-listing.twig';

import cardListingData from './card-listing.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    component_heading: { control: 'text' },
    all_link__url: { control: 'text' },
    all_link__text: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return cardListingTemplate({ label, ...args });
};

export const cardListing = Template.bind({});
cardListing.args = {
  card_listing: cardListingData,
  component_heading: 'Related News',
  all_link__url: '#',
  all_link__text: 'All News',
};
