import eventListingTemplate from './event-listing.twig';

import eventListingData from './event-listing.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    component_heading: { control: 'text' },
    all_link__url: { control: 'text' },
    all_link__text: { control: 'text' },
    border: {
      control: 'select',
      options: ['top', 'bottom', 'none'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return eventListingTemplate({ label, ...args });
};

export const eventListing = Template.bind({});
eventListing.args = {
  event_listing: eventListingData.events,
  component_heading: 'Featured Events',
  all_link__url: '#',
  all_link__text: 'All Events',
  border: 'top',
};
