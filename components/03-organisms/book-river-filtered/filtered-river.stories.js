import filteredRiverTemplate from './filtered-river.twig';
import filteredRiverData from './filtered-river.yml';
import './filtered-river';

/**
 * Storyfiltered Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    filters: {
      control: { type: 'array' },
    },
    section_title: { control: 'text' },
    cta: { control: 'text' },
    background: { control: 'boolean' },
  },
};

const Template = ({ label, ...args }) => {
  return filteredRiverTemplate({ label, ...args });
};

export const BookRiverFiltered = Template.bind({});
BookRiverFiltered.args = {
  background: true,
  section_title: 'Book Club Picks',
  rivers_data: filteredRiverData,
};
