(function riverTabs() {
  Drupal.behaviors.riverTabs = {
    attach() {
      const component = document.getElementsByClassName('rivers-container');
      const river = document.getElementsByClassName('book-river');
      const tabs = document.getElementsByClassName('tab');
      const tabLinks = document.getElementsByClassName('tab-link');
      const firstTab = tabLinks[0];
      const firstRiver = river[0];

      if (component && firstTab) {
        // Add active class to first button.
        firstTab.classList.add('active');

        // Hide all rivers.
        Array.from(river).forEach((item) => {
          item.classList.add('hidden');
        });

        // Show first river.
        firstRiver.classList.remove('hidden');

        // Click function.
        Array.from(tabs).forEach((selectedItem) => {
          selectedItem.addEventListener('click', function toggleTab(e) {
            e.preventDefault();

            // Hide all rivers.
            Array.from(river).forEach((item) => {
              item.classList.add('hidden');
            });

            // Loop through top level tabs.
            Array.from(tabs).forEach(() => {
              const indexOfTab = Array.from(tabs).indexOf(this);
              const tabNumber = indexOfTab + 1;
              const riverTab = 'book-river-' + tabNumber; // eslint-disable-line prefer-template
              const tabRiver = document.getElementsByClassName(riverTab);
              const child = this.firstElementChild;

              // Change active class and toggle shown river.
              Array.from(tabLinks).forEach((link) => {
                link.classList.remove('active');
              });

              child.classList.add('active');
              tabRiver[0].classList.remove('hidden');

              // Change Section header button based on active tab.
              const viewAllBtn = document.querySelector(
                '.filtered-river .section__header .button--secondary',
              );
              if (child.nextElementSibling) {
                const riverlLink =
                  child.nextElementSibling.getAttribute('data-link');
                const riverlLinkText = child.nextElementSibling.innerText;

                if (viewAllBtn) {
                  viewAllBtn.remove();
                }
                const sectionLinkButton = `<a class="button button--secondary" href="${riverlLink}">${riverlLinkText}</a>`;
                document
                  .querySelector('.filtered-river .section__header')
                  .insertAdjacentHTML('beforeend', sectionLinkButton);
              } else {
                viewAllBtn.remove();
              }
            });
          });
        });
      }
    },
  };
})(Drupal);
