import cardGroupTemplate from './card-group.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    columns: {
      options: [2, 3, 4],
      control: { type: 'select' },
    },
    cards: {
      control: { type: 'array' },
    },
  },
};

const Template = ({ label, ...args }) => {
  return cardGroupTemplate({ label, ...args });
};

export const cardGroup = Template.bind({});
cardGroup.args = {
  columns: 2,
  group: [
    {
      card__image__src: 'https://picsum.photos/900/600',
      card__image__alt: "This is the card image's alt text",
      card__image__output_image_tag: true,
      card__heading: 'H3 Article title',
      card__body:
        'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill. Help us keep Mesa County clean — tarp it!',
      card__date: 'May 14, 2022',
      card__tags__icon: 'tags',
      card__tags: 'County News, Safety',
      card__link__url: '#',
    },
    {
      card__image__src: 'https://picsum.photos/900/600',
      card__image__alt: "This is the card image's alt text",
      card__image__output_image_tag: true,
      card__heading: 'Another title',
      card__body:
        'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill.',
      card__date: 'May 14, 2022',
      card__tags__icon: 'tags',
      card__tags: 'County News, Safety',
      card__link__url: '#',
    },
    {
      card__image__src: 'https://picsum.photos/900/600',
      card__image__alt: "This is the card image's alt text",
      card__image__output_image_tag: true,
      card__heading: 'H3 Article title',
      card__body:
        'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill. Help us keep Mesa County clean — tarp it!',
      card__date: 'May 14, 2022',
      card__tags__icon: 'tags',
      card__tags: 'County News, Safety',
      card__link__url: '#',
    },
    {
      card__image__src: 'https://picsum.photos/900/600',
      card__image__alt: "This is the card image's alt text",
      card__image__output_image_tag: true,
      card__heading: 'Another title',
      card__body:
        'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill.',
      card__date: 'May 14, 2022',
      card__tags__icon: 'tags',
      card__tags: 'County News, Safety',
      card__link__url: '#',
    },
    {
      card__image__src: 'https://picsum.photos/900/600',
      card__image__alt: "This is the card image's alt text",
      card__image__output_image_tag: true,
      card__heading: 'H3 Article title',
      card__body:
        'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill. Help us keep Mesa County clean — tarp it!',
      card__date: 'May 14, 2022',
      card__tags__icon: 'tags',
      card__tags: 'County News, Safety',
      card__link__url: '#',
    },
    {
      card__image__src: 'https://picsum.photos/900/600',
      card__image__alt: "This is the card image's alt text",
      card__image__output_image_tag: true,
      card__heading: 'Another title',
      card__body:
        'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill.',
      card__date: 'May 14, 2022',
      card__tags__icon: 'tags',
      card__tags: 'County News, Safety',
      card__link__url: '#',
    },
  ],
};
