import buttonGroupTemplate from './button-group.twig';
import buttonGroupData from './button-group.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    columns: {
      options: [2, 3, 4],
      control: { type: 'select' },
    },
    icon_cards: {
      control: { type: 'array' },
    },
  },
};

const Template = ({ label, ...args }) => {
  return buttonGroupTemplate({ label, ...args });
};

export const buttonGroup = Template.bind({});
buttonGroup.args = {
  columns: 4,
  buttons: buttonGroupData.items,
};
