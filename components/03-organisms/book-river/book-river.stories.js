import bookRiverTemplate from './book-river.twig';
import bookRiverData from './book-river.yml';
import './book-river';

/**
 * Storybook Definition.
 */
export default {
  title: 'Organisms',
  argTypes: {
    books: {
      control: { type: 'array' },
    },
    section_title: { control: 'text' },
    cta: { control: 'text' },
    background: { control: 'boolean' },
    margin: {
      control: { type: 'radio' },
      options: ['default', 'zero', 'small', 'medium', 'large', 'top'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return bookRiverTemplate({ label, ...args });
};

export const BookRiver = Template.bind({});
BookRiver.args = {
  section_title: 'Book Club Picks',
  background: false,
  books: bookRiverData.items,
};
