(function scrollButton() {
  Drupal.behaviors.scrollButton = {
    attach() {
      const button = document.getElementsByClassName('scroll-button');
      const river = document.getElementsByClassName('book-river');

      Array.from(button).forEach((item) => {
        item.addEventListener('click', function scroll() {
          this.classList.add('hidden');

          Array.from(river).forEach((container) => {
            const scrollArea = container;

            scrollArea.scrollLeft += 400;
          });
        });
      });
    },
  };
})(Drupal);
