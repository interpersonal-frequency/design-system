import '../../02-molecules/menus/main-menu/main-menu';

import home from './home.twig';
import landingPageTemplate from './landing-page.twig';

import featuredEventsData from './featured-events.yml';
import featuredServicesData from './featured-services.yml';
import footerMenuData from '../../02-molecules/menus/inline/inline-menu.yml';
import footerData from '../../03-organisms/site/site-footer/site-footer.yml';
import heroHeaderData from './lp-hero-header.yml';
import homeHeroHeaderData from './home-hero-header.yml';
import iconCardData from './lp-icon-card.yml';
import iconCardGroupData from './lp-icon-card-group.yml';
import iWantToData from './i-want-to.yml';
import newsData from './news.yml';
import mainMenuData from '../../02-molecules/menus/main-menu/main-menu.yml';
import socialMenuData from '../../02-molecules/social-links/social-links.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Pages/Landing Pages',
  parameters: {
    layout: 'fullscreen',
  },
};

export const homePage = () =>
  home({
    page_layout_modifier: 'contained',
    ...mainMenuData,
    ...socialMenuData,
    ...footerMenuData,
    ...footerData,
    homeHeroHeaderData,
    featuredEventsData,
    featuredServicesData,
    iWantToData,
    newsData,
    card__link__text: 'Click here',
  });

export const landingPage = () =>
  landingPageTemplate({
    page_layout_modifier: 'contained',
    ...mainMenuData,
    ...socialMenuData,
    ...footerMenuData,
    ...footerData,
    iconCardData,
    heroHeaderData,
    iconCardGroupData,
  });
