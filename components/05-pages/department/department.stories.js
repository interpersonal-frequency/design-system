import breadcrumbs from '../../02-molecules/menus/breadcrumbs/breadcrumbs.yml';
import cardListingData from '../../03-organisms/card-listing/card-listing.yml';
import department from './department.twig';
import eventsData from '../../03-organisms/event-listing/event-listing.yml';
import footerData from '../../03-organisms/site/site-footer/site-footer.yml';
import headerA from './department-header-a.yml';
import headerB from './department-header-b.yml';
import iconCardGroupA from './icon-card-group-a.yml';
import iconCardGroupB from './icon-card-group-b.yml';
import mainMenuData from '../../02-molecules/menus/main-menu/main-menu.yml';
import addresses from '../../02-molecules/location-map/addresses.yml';
import socialMenu from '../../02-molecules/social-links/social-links.yml';

/* Storybook definition */
export default {
  title: 'Pages/Departments',
  parameters: {
    layout: 'fullscreen',
  },
};

export const departmentA = () =>
  department({
    ...footerData,
    ...mainMenuData,
    addresses,
    breadcrumbs,
    cardListingData,
    eventsData,
    iconCardGroupA,
    headerA,
    socialMenu,
  });

export const departmentB = () =>
  department({
    ...footerData,
    ...mainMenuData,
    addresses,
    breadcrumbs,
    cardListingData,
    eventsData,
    iconCardGroupB,
    headerB,
    socialMenu,
  });
