import basicPageTemplate from './basic-page.twig';
import footerData from '../../03-organisms/site/site-footer/site-footer.yml';
import sidebarNavMenu from '../../02-molecules/menus/sidebar-menu/sidebar-menu.yml';
import accordionItems from '../../02-molecules/accordions/accordions.yml';
import steps from '../../02-molecules/process-step/process-step.yml';
import photoGallery from './photo-gallery.yml';
import breadcrumbs from '../../02-molecules/menus/breadcrumbs/breadcrumbs.yml';
import iconCardGroup from './icon-card-group.yml';
import sectionNews from './section-news.yml';
import sectionEvents from './section-events.yml';

/* Storybook definition */
export default {
  title: 'Pages',
  parameters: {
    layout: 'fullscreen',
  },
};

export const basicPage = () =>
  basicPageTemplate({
    ...sidebarNavMenu,
    ...footerData,
    accordionItems,
    steps,
    photoGallery,
    breadcrumbs,
    iconCardGroup,
    sectionNews,
    sectionEvents,
  });
