import '../../02-molecules/menus/main-menu/main-menu';

import breadcrumbs from '../../02-molecules/menus/breadcrumbs/breadcrumbs.yml';
import eventDetailTemplate from './event-detail.twig';
import eventCardData from './event-card.yml';
import footerData from '../../03-organisms/site/site-footer/site-footer.yml';
import locationData from './location.yml';
import mainMenuData from '../../02-molecules/menus/main-menu/main-menu.yml';
import socialMenuData from '../../02-molecules/social-links/social-links.yml';

/* Storybook definition */
export default {
  title: 'Pages',
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    no__image: {
      control: 'boolean',
    },
  },
};

export const eventDetail = () =>
  eventDetailTemplate({
    breadcrumbs,
    eventCardData,
    locationData,
    ...footerData,
    ...socialMenuData,
    ...mainMenuData,
    img__src: 'https://picsum.photos/500/400',
    img__alt: 'Image alt text',
  });
