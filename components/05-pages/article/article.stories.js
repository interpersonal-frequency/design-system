import articleTemplate from './article.twig';
import breadcrumbs from '../../02-molecules/menus/breadcrumbs/breadcrumbs.yml';
import callOutBox from '../../02-molecules/call-out-box/call-out-box.yml';
import footerData from '../../03-organisms/site/site-footer/site-footer.yml';

/* Storybook definition */
export default {
  title: 'Pages',
  parameters: {
    layout: 'fullscreen',
  },
};

export const article = () =>
  articleTemplate({
    breadcrumbs,
    callOutBox,
    ...footerData,
  });
