import searchResultsTemplate from './search-results.twig';
import pagerData from './pager.yml';
import filterData from './filter-data.yml';
import footerMenuData from '../../02-molecules/menus/inline/inline-menu.yml';
import footerData from '../../03-organisms/site/site-footer/site-footer.yml';
import mainMenuData from '../../02-molecules/menus/main-menu/main-menu.yml';
import socialMenuData from '../../02-molecules/social-links/social-links.yml';
import selectData from '../../01-atoms/forms/select/select.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Pages/Search Results',
  parameters: {
    layout: 'fullscreen',
  },
};

export const searchResults = () =>
  searchResultsTemplate({
    page_layout_modifier: 'contained',
    ...mainMenuData,
    ...socialMenuData,
    ...footerMenuData,
    ...footerData,
    ...selectData,
    pagerData,
    filterData,
  });
