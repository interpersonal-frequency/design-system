import icon from './icon.twig';
import iconTypes from './icon-types.yml';

export default {
  title: 'Atoms',
  argTypes: {
    icon_type: {
      control: {
        type: 'select',
        options: iconTypes,
      },
    },
    icon_name: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  // You can either use a function to create DOM elements or use a plain html string!
  // return `<div>${label}</div>`;
  return icon({ label, ...args });
};

export const icons = Template.bind({});
icons.args = {
  icon_name: 'recycle',
  icon_type: 'regular',
  icon_modifiers: [],
};
