import link from './link.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Atoms/Links',
  argTypes: {
    link_content: { control: 'text' },
    link_url: { control: 'text' },
    link_attributes: { control: 'object' },
  },
};

const Template = ({ label, ...args }) => {
  // You can either use a function to create DOM elements or use a plain html string!
  // return `<div>${label}</div>`;
  return link({ label, ...args });
};

export const inlineLink = Template.bind({});
inlineLink.args = {
  link_url: 'https://github.com/emulsify-ds/emulsify-design-system',
  link_content: 'This is my link text',
  link_attributes: {
    target: '_blank',
  },
};

export const darkLink = Template.bind({});
darkLink.args = {
  link_url: '#',
  link_content: 'This is my link text',
  link_attributes: {},
  link_modifiers: ['dark'],
};

export const largeLink = Template.bind({});
largeLink.args = {
  link_url: '#',
  link_content: 'This is my link text',
  link_attributes: {},
  link_modifiers: ['large'],
};
