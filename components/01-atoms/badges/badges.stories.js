import badge from './badge.twig';
import iconTypes from '../icon/icon-types.yml';

export default {
  title: 'Atoms',
  argTypes: {
    badge_text: { control: 'text' },
    badge_label: { control: 'text' },
    isLighter: { control: 'boolean' },
    badge_icon_type: {
      control: {
        type: 'select',
        options: iconTypes,
      },
    },
  },
};

const Template = ({ label, ...args }) => {
  // You can either use a function to create DOM elements or use a plain html string!
  // return `<div>${label}</div>`;
  return badge({ label, ...args });
};

export const badges = Template.bind({});
badges.args = {
  badge_icon: 'user',
  badge_icon_type: 'solid',
  badge_label: 'Label',
  badge_text: 'badge text',
  isLighter: false,
  badge_modifiers: [],
};
