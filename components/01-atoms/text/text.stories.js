import paragraph from './text/01-paragraph.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Atoms/Text',
  argTypes: {
    copy: { control: 'text' },
    dark: { control: 'boolean' },
  },
};

const TextTemplate = ({ label, ...args }) => {
  return paragraph({ label, ...args });
};

export const paragraphs = TextTemplate.bind({});
paragraphs.args = {
  copy:
    'Body copy/paragraph<br/>' +
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br/>' +
    'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.<br/>' +
    'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
  dark: false,
};
