import heading from './_heading.twig';

const HeadingLevels = {
  1: '1',
  2: '2',
  3: '3',
  4: '4',
  5: '5',
};

export default {
  title: 'Atoms/Text/Headings',
  argTypes: {
    heading_level: { control: { type: 'select', options: HeadingLevels } },
    heading: { control: 'text' },
    dark: { control: 'boolean' },
  },
};

const HeadingsTemplate = ({ label, ...args }) => {
  return heading({ label, ...args });
};

export const Heading1 = HeadingsTemplate.bind({});
Heading1.args = {
  heading_level: 1,
  heading: 'Headline One / Inter SemiBold',
  dark: false,
};
export const Heading2 = HeadingsTemplate.bind({});
Heading2.args = {
  heading_level: 2,
  heading: 'Headline Two / Inter ExtraBold',
  dark: false,
};
export const Heading3 = HeadingsTemplate.bind({});
Heading3.args = {
  heading_level: 3,
  heading: 'Headline Three / Inter SemiBold',
  dark: false,
};
export const Heading4 = HeadingsTemplate.bind({});
Heading4.args = {
  heading_level: 4,
  heading: 'Headline Four / Inter SemiBold',
  dark: false,
};
export const Heading5 = HeadingsTemplate.bind({});
Heading5.args = {
  heading_level: 5,
  heading: 'Headline Five / Inter SemiBold',
  dark: false,
};
