import button from './button.twig';
import iconTypes from '../icon/icon-types.yml';

export default {
  title: 'Atoms/Button',
  argTypes: {
    button_content: { control: 'text' },
    button_url: { control: 'text' },
    button_link_attributes: { control: 'object' },
    isDisabled: { control: 'boolean' },
    isActive: { control: 'boolean' },
    button_icon: { control: 'text' },
    cta: { control: 'boolean' },
    button_icon_type: {
      control: {
        type: 'select',
        options: iconTypes,
      },
    },
  },
};

const Template = ({ label, ...args }) => {
  // You can either use a function to create DOM elements or use a plain html string!
  // return `<div>${label}</div>`;
  return button({ label, ...args });
};

export const buttonPrimary = Template.bind({});
buttonPrimary.args = {
  cta: false,
  button_content: 'Button',
  button_url: '#',
  button_link_attributes: {},
  isDisabled: false,
  isActive: false,
  button_modifiers: ['primary'],
};

export const buttonSecondary = Template.bind({});
buttonSecondary.args = {
  cta: false,
  button_content: 'Button',
  button_url: '#',
  button_link_attributes: {},
  isDisabled: false,
  isActive: false,
  button_modifiers: ['secondary'],
};
