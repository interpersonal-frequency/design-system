import date from './date.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Date' };

export const DateInput = () => date();
