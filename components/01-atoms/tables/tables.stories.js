import tables from './tables.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms' };

export const table = () => tables();
