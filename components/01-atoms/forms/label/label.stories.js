import formLabel from './label.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Atoms/Forms/Label',
  argTypes: {
    text: { control: 'text' },
    forAttribute: { control: 'text' },
  },
};

const LabelTemplate = ({ label, ...args }) => {
  return formLabel({ label, ...args });
};

export const label = LabelTemplate.bind({});
label.args = {
  text: 'Default Label',
};
