import resourceCardTemplate from './resource-card.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    card_img_src: { control: 'text' },
    card_img_alt: { control: 'text' },
    card_link: { control: 'text' },
    card_heading: { control: 'text' },
    card_audience: { control: 'text' },
    card_type: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return resourceCardTemplate({ label, ...args });
};

export const resourceCard = Template.bind({});
resourceCard.args = {
  card_img_src: 'https://picsum.photos/900/600',
  card_img_alt: 'Image alt text',
  card_body:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris.',
  card_link: '#',
  card_heading: 'Card Heading',
  card_audience: 'Adults',
  card_type: 'Database',
};
