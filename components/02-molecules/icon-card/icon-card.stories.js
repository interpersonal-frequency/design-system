import iconcard from './icon-card.twig';
import iconcardData from './icon-card.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    showDescription: { control: 'boolean' },
    showCTAArrow: { control: 'boolean' },
    linked: { control: 'boolean' },
    icon_card_content: { control: 'object' },
    icon_color: {
      control: 'select',
      options: [
        'primary-1',
        'primary-2',
        'primary-3',
        'accent-1',
        'tone-1',
        'tone-9',
      ],
    },
    text_color: {
      control: 'select',
      options: [
        'primary-1',
        'primary-2',
        'primary-3',
        'tone-9',
        'tone-8',
        'tone-1',
      ],
    },
  },
};

const Template = ({ label, ...args }) => {
  return iconcard({ label, ...args });
};

export const iconCard = Template.bind({});
iconCard.args = {
  showDescription: true,
  showCTAArrow: false,
  linked: true,
  icon_card_content: iconcardData,
};
