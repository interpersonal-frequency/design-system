import processStepTemplate from './process-step.twig';
import processStepData from './process-step.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    steps: {
      control: { type: 'array' },
    },
    margin: {
      control: { type: 'radio' },
      options: ['default', 'zero', 'small', 'medium', 'large', 'top'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return processStepTemplate({ label, ...args });
};

export const ProcessStep = Template.bind({});
ProcessStep.args = {
  steps: processStepData.process_steps,
  margin: 'default',
};
