(function howDoI() {
  Drupal.behaviors.howDoI = {
    attach() {
      const menu = document.getElementsByClassName('how-do-i');
      const subMenu = document.getElementsByClassName('menu');
      const tabs = document.getElementsByClassName('tab');
      const tabLinks = document.getElementsByClassName('tab-link');
      const firstTab = tabLinks[0];
      const firstSubMenu = subMenu[0];

      if (menu && firstTab) {
        // Add active class to first button.
        firstTab.classList.add('active');

        // Hide all submenus.
        Array.from(subMenu).forEach((item) => {
          item.classList.add('hidden');
        });

        // Show first submenu.
        firstSubMenu.classList.remove('hidden');

        // Click function.
        Array.from(tabs).forEach((selectedItem) => {
          selectedItem.addEventListener('click', function toggleTab(e) {
            e.preventDefault();

            // Hide all submenus.
            Array.from(subMenu).forEach((item) => {
              item.classList.add('hidden');
            });

            const index = Array.from(tabs).indexOf(selectedItem);
            const menuTab = `menu-tab-${index + 1}`;
            const tabMenu = document.querySelector(`.${menuTab}`);
            const child = selectedItem.firstElementChild;

            Array.from(tabLinks).forEach((link) => {
              link.classList.remove('active');
            });

            child.classList.add('active');
            tabMenu.classList.remove('hidden');
          });
        });

        // Keydown function for accessibility
        Array.from(tabs).forEach((selectedItem) => {
          selectedItem.addEventListener(
            'keydown',
            function tabKeydownHandler(e) {
              if (e.shiftKey && e.key === 'Tab') {
                // Check if submenu is open
                const openSubMenu =
                  document.querySelector('.menu:not(.hidden)');
                if (openSubMenu) {
                  // Find parent tab and focus
                  const parentTab = openSubMenu.closest('.tab');
                  if (parentTab) {
                    parentTab.focus();
                  }
                }
              } else if (e.key === 'ArrowDown') {
                e.preventDefault();
                const activeTabIndex = Array.from(tabs).indexOf(selectedItem);
                const menuTab = `menu-tab-${activeTabIndex + 1}`;
                const tabMenu = document.querySelector(`.${menuTab}`);
                const firstLink = tabMenu.querySelectorAll('a')[0];
                if (tabMenu && firstLink) {
                  firstLink.focus();
                }
              }
            },
          );
        });

        // Keydown function for submenu navigation
        Array.from(subMenu).forEach((sub) => {
          const subLinks = sub.querySelectorAll('a');
          Array.from(subLinks).forEach((subLink) => {
            subLink.addEventListener(
              'keydown',
              function subLinkKeydownHandler(e) {
                if (e.key === 'ArrowDown' || e.key === 'ArrowRight') {
                  e.preventDefault();
                  const nextElement = this.nextElementSibling;
                  if (nextElement) {
                    nextElement.focus();
                  } else {
                    const activeTabLink = sub.querySelector('.tab-link.active');
                    if (activeTabLink) {
                      activeTabLink.focus();
                    }
                  }
                } else if (
                  e.key === 'ArrowUp' ||
                  e.key === 'ArrowLeft' ||
                  (e.shiftKey && e.key === 'Tab')
                ) {
                  e.preventDefault();
                  const previousElement = this.previousElementSibling;
                  if (previousElement) {
                    previousElement.focus();
                  } else {
                    const activeTabLink =
                      document.querySelector('.tab-link.active');
                    if (activeTabLink) {
                      activeTabLink.focus();
                    }
                  }
                }
              },
            );
          });
        });
      }
    },
  };
})(Drupal);
