import howDoITemplate from './how-do-i.twig';
import howDoIData from './how-do-i.yml';
import './how-do-i';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    tabs: {
      control: { type: 'array' },
    },
  },
};

const Template = ({ label, ...args }) => {
  return howDoITemplate({ label, ...args });
};

export const howDoI = Template.bind({});
howDoI.args = {
  tabs: howDoIData.tab_items,
};
