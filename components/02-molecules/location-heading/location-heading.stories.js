import locationHeadingTemplate from './location-heading.twig';
import './location-heading-example';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Location Heading',
  argTypes: {
    heading: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return locationHeadingTemplate({ label, ...args });
};

export const locationHeading = Template.bind({});
locationHeading.args = {
  heading: 'Hours & Locations',
};
