(function closeAlert() {
  Drupal.behaviors.closeAlert = {
    attach() {
      const close = document.getElementsByClassName('close');
      const alertMessage = document.getElementsByClassName('alerts');

      Array.from(close).forEach((button) => {
        button.addEventListener('click', function hide() {
          Array.from(alertMessage).forEach((component) => {
            component.classList.add('hidden');
          });
        });
      });
    },
  };
})(Drupal);
