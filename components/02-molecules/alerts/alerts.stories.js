import alertsTemplate from './alerts.twig';
import alertsData from './alerts.yml';
import './alerts';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    alert_level: {
      options: ['Informational', 'Non-Emergency', 'Emergency'],
      control: { type: 'select' },
    },
    dismissible: {
      control: { type: 'boolean' },
    },
    icon: { type: 'text' },
    title: { type: 'text' },
    body: { type: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return alertsTemplate({ label, ...args });
};

export const alerts = Template.bind({});
alerts.args = {
  dismissible: true,
  alert_level: alertsData.alert_level,
  icon: alertsData.icon,
  title: alertsData.title,
  body: alertsData.body,
};
