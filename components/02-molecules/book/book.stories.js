import book from './book.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    title: { control: 'text' },
    url: { control: 'text' },
    author: { control: 'text' },
    image_src: { control: 'text' },
    image_alt: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return book({ label, ...args });
};

export const Book = Template.bind({});
Book.args = {
  title: 'The Summer I Turned Pretty',
  url: '#',
  author: 'Jenny Han',
  image_src: 'https://images.pexels.com/photos/532310/pexels-photo-532310.jpeg',
  image_alt: 'Book cover',
};
