import richText from './rich-text.twig';
import './rich-text';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    body: { control: 'text' },
    hasBgColor: { control: 'boolean' },
    margin: {
      control: { type: 'radio' },
      options: ['default', 'zero', 'small', 'medium', 'large', 'top'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return richText({ label, ...args });
};

export const RichText = Template.bind({});
RichText.args = {
  margin: 'default',
  hasBgColor: false,
};
