Drupal.behaviors.richText = {
  attach() {
    function prepStickyTables() {
      // figure out if there's a sticky site header
      let hdelta = 0;
      const header = document.querySelector('.header');
      if (header) {
        const hHeight = header.getBoundingClientRect().height;
        const hPosition = getComputedStyle(header).getPropertyValue('position');
        if (hPosition === 'fixed') {
          // it's a sticky header
          hdelta = hHeight;
        }
      }
      // figure out if there's a Drupal admin bar that's taller than the (potentially) fixed site header
      if (document.body.classList.contains('toolbar-fixed')) {
        const barH = parseFloat(
          getComputedStyle(document.body).getPropertyValue('padding-top'),
        );
        if (!Number.isNaN(barH) && barH > hdelta) hdelta = barH;
      }

      // figure out dimensions for all tables that can be stickied
      const tableInstances = document.querySelectorAll('.rich-text table');
      Array.from(tableInstances).forEach((item) => {
        item.style.setProperty('--sticky-header-offset', `${hdelta}px`);
      });
    }

    window.addEventListener('DOMContentLoaded', prepStickyTables);
    window.addEventListener('resize', prepStickyTables);
  },
};
