import section from './section.twig';

export default {
  title: 'Molecules',
  argTypes: {
    showTitle: { control: 'boolean' },
    heading: { control: 'text' },
    button_value: { control: 'text' },
    button_link: { control: 'text' },
    bg_color: {
      control: { type: 'radio' },
      options: ['tone-1', 'primary-4'],
    },
    padding: {
      control: { type: 'radio' },
      options: ['regular', 'reduced', 'none', 'top', 'bottom'],
    },
    margin: {
      control: { type: 'radio' },
      options: ['default', 'zero', 'small', 'medium', 'large', 'top'],
    },
    no_flexgrid: { type: 'boolean' },
    component_level_modifiers: { type: 'text' },
  },
};

const Template = ({ ...args }) => {
  return section({ ...args });
};

export const Section = Template.bind({});
Section.args = {
  showTitle: true,
  heading: 'Section Title',
  button_value: 'CTA Link',
  button_link: '#',
  bg_color: 'tone-1',
  margin: 'default',
  no_flexgrid: false,
};
