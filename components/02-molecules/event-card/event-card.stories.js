import eventCardTemplate from './event-card.twig';

import eventCardData from './event-card.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    border: {
      control: 'select',
      options: ['top', 'bottom', 'none'],
    },
    no__image: {
      control: 'boolean',
    },
  },
};

const Template = ({ label, ...args }) => {
  return eventCardTemplate({ label, ...args });
};

export const eventCard = Template.bind({});
eventCard.args = {
  border: 'top',
  event: eventCardData,
  img__src: 'https://picsum.photos/500/400',
  img__alt: 'Image alt text',
  no__image: false,
};
