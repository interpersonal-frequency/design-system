import photoGallery from './photo-gallery.twig';
import photoGalleryItems from './photo-gallery.yml';

export default {
  title: 'Molecules',
  argTypes: {
    items: { control: 'array' },
  },
};

const Template = ({ label, ...args }) => {
  return photoGallery({ label, ...args });
};

export const PhotoGallery = Template.bind({});
PhotoGallery.args = {
  items: photoGalleryItems.gallery_items,
};
