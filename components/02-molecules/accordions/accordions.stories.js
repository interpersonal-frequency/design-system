import accordions from './accordions.twig';
import accordionData from './accordions.yml';
import './accordions';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    margin: {
      control: { type: 'radio' },
      options: ['default', 'zero', 'small', 'medium', 'large', 'top'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return accordions({ label, ...args });
};

export const AccordionGroup = Template.bind({});
AccordionGroup.args = {
  accordion: accordionData.accordion_items,
  margin: 'default',
};
