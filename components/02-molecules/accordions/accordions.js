Drupal.behaviors.accordions = {
  attach() {
    function accordize(target) {
      const accordionItems = target.getElementsByClassName('accordion-item');
      const textContent = document.getElementsByClassName('accordion-text');

      Array.from(accordionItems).forEach((selectedItem) => {
        selectedItem.addEventListener('click', function toggleAccordion() {
          Array.from(accordionItems).forEach((item) => {
            if (item !== this) {
              item.classList.remove('open');
              item.setAttribute('aria-expanded', 'false');
            } else if (this.classList.contains('open')) {
              this.classList.remove('open');
              this.setAttribute('aria-expanded', 'false');
            } else {
              this.classList.add('open');
              this.setAttribute('aria-expanded', 'true');
            }
          });

          Array.from(textContent).forEach((text) => {
            const previousSibling = text.previousElementSibling;
            if (previousSibling && previousSibling.classList.contains('open')) {
              text.removeAttribute('hidden');
            } else {
              text.setAttribute('hidden', '');
            }
          });
        });
      });
    }

    window.addEventListener('DOMContentLoaded', function startAcc() {
      const accordions = document.getElementsByClassName('accordion-group');
      Array.from(accordions).forEach((item) => {
        accordize(item, true);
      });
    });
  },
};
