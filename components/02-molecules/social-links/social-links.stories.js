import socialLinksTemplate from './social-links.twig';
import socialLinksData from './social-links.yml';

export default {
  title: 'Molecules/Social Links',
  argTypes: {
    modifiers: { control: 'array' },
  },
};

const Template = ({ label, ...args }) => {
  return socialLinksTemplate({ label, ...args });
};

export const socialLinksRegular = Template.bind({});
socialLinksRegular.args = {
  modifiers: ['regular'],
  social_menu_items: socialLinksData.social_menu_items,
};

export const socialLinksBig = Template.bind({});
socialLinksBig.args = {
  modifiers: ['big'],
  social_menu_items: socialLinksData.social_menu_items,
};
