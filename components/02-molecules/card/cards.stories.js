import cardTemplate from './card.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    background: { type: 'boolean' },
    card__image__src: { type: 'text' },
    card__image__alt: { type: 'text' },
    card__image__output_image_tag: { type: 'boolean' },
    card__heading: { type: 'text' },
    card__body: { type: 'text' },
    card__date: { type: 'text' },
    card__tags__icon: { type: 'text' },
    card__tags: { type: 'text' },
    card__link__url: { type: 'text' },
    card__type__icon: { type: 'text' },
    card__type: { type: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return cardTemplate({ label, ...args });
};

export const Card = Template.bind({});
Card.args = {
  background: true,
  card__image__src: 'https://placeimg.com/900/600/nature',
  card__image__alt: "This is the card image's alt text",
  card__image__output_image_tag: true,
  card__heading: 'H3 Article title',
  card__body:
    'Lately, it’s been super windy, so don’t forget to tarp your trash as you drive to the landfill. Help us keep Mesa County clean — tarp it!',
  card__date: 'May 14, 2022',
  card__tags__icon: 'tags',
  card__tags: 'County News, Safety',
  card__link__url: '#',
  card__type__icon: 'folders',
  card__type: 'Database',
};
