import statCardTemplate from './stat-card.twig';
import statCardData from './stat-card.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    stat: { control: 'text' },
    description: { control: 'text' },
    icon_name: { control: 'text' },
    icon_color: {
      control: 'select',
      options: ['tone-9', 'accent-1', 'primary-3', 'warning', 'success'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return statCardTemplate({ label, ...args });
};

export const StatCard = Template.bind({});
StatCard.args = {
  icon_name: 'shopping-bag',
  icon_color: 'primary-3',
  stat_card: statCardData,
};
