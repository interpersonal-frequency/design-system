import horizontalCardTemplate from './horizontal-card.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    border: {
      control: 'select',
      options: ['top', 'bottom'],
    },
  },
};

const Template = ({ label, ...args }) => {
  return horizontalCardTemplate({ label, ...args });
};

export const horizontalCard = Template.bind({});
horizontalCard.args = {
  border: 'bottom',
  horizontal_card__image__src: 'https://placeimg.com/900/600/nature',
  horizontal_card__image__alt: "This is the card image's alt text",
  horizontal_card__image__output_image_tag: true,
  horizontal_card__heading: 'H3 Article title',
  horizontal_card__summary: 'This is the card summary',
  horizontal_card__date: 'October 4, 2022',
  horizontal_card__tags: 'Adults, Teens, Kids',
  horizontal_card__tags__icon: 'tags',
  horizontal_card__type: 'Website',
  horizontal_card__type__icon: 'folders',
  horizontal_card__link__url: '#',
};
