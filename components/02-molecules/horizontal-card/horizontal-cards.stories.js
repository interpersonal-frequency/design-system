import horizontalCardTemplate from './horizontal-card.twig';

import horizontalCardData from './horizontal-card.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
};

const Template = ({ label, ...args }) => {
  return horizontalCardTemplate({ label, ...args });
};

export const horizontalCard = Template.bind({});
horizontalCard.args = {
  horizontal_card: horizontalCardData,
};
