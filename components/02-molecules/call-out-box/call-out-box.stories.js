import callOutBoxTemplate from './call-out-box.twig';

import callOutBoxData from './call-out-box.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    html_content: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return callOutBoxTemplate({ label, ...args });
};

export const callOutBox = Template.bind({});
callOutBox.args = {
  html_content: callOutBoxData.content,
};
