import authorEventCardTemplate from './author-event-card.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Cards',
  argTypes: {
    isHorizontal: { control: 'boolean' },
    registration: { control: 'boolean' },
    card_img_src: { control: 'text' },
    card_img_alt: { control: 'text' },
    card_link: { control: 'text' },
    card_heading: { control: 'text' },
    card_body: { control: 'text' },
    card_date: { control: 'text' },
    card_time: { control: 'text' },
    card_location: { control: 'text' },
    card_location_url: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return authorEventCardTemplate({ label, ...args });
};

export const authorEventCard = Template.bind({});
authorEventCard.args = {
  card_img_src: 'https://placeimg.com/840/700/nature',
  card_img_alt: 'Image alt text',
  card_link: '#',
  card_heading: 'Card Heading',
  card_date: 'July 4, 2023',
  card_time: '12:00pm',
  card_location: '1234 Main St, Anytown, USA',
  card_location_url: '#',
  card_body: 'This is the card summary',
  isHorizontal: false,
  registration: false,
};
