import pageHeader from './page-header.twig';
/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Page Headers',
  argTypes: {
    heading: { control: 'text' },
    date: { control: 'text' },
    hide_date: { control: 'boolean' },
  },
};

const Template = ({ label, ...args }) => {
  return pageHeader({ label, ...args });
};

export const headerPage = Template.bind({});
headerPage.args = {
  heading: 'Page header',
  date: 'September, 30th',
  hide_date: false,
};
