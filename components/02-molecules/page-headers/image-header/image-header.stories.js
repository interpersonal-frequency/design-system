import imageHeader from './image-header.twig';
import menuLinks from './image-header-menu.yml';
import headerContent from './header-content.yml';
/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Page Headers',
  argTypes: {
    heading: { control: 'text' },
    subheading: { control: 'text' },
    menu_links: { control: 'array' },
    image_src: { control: 'text' },
    image_alt: { control: 'text' },
    image_bio_name: { control: 'text' },
    image_bio_title: { control: 'text' },
    hide_image_bio: { control: 'boolean' },
    modifiers: { control: 'array' },
  },
  parameters: {
    layout: 'fullscreen',
  },
};

const Template = ({ label, ...args }) => {
  return imageHeader({ label, ...args });
};

export const headerImage = Template.bind({});
headerImage.args = {
  heading: 'Image header',
  image_alt: 'Image',
  image_bio_name: 'John Smith',
  image_bio_title: 'County Mayor',
  hide_image_bio: true,
  menu_links: menuLinks,
  modifiers: ['button-links'],
  header_content: headerContent,
};
