import homeHeaderTemplate from './home-header.twig';
import homeHeaderData from './home-header.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Page Headers',
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    image: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return homeHeaderTemplate({ label, ...args });
};

export const homeHeader = Template.bind({});
homeHeader.args = {
  image: 'https://via.placeholder.com/2500x750',
  home_header_data: homeHeaderData,
};
