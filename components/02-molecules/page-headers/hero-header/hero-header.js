Drupal.behaviors.heroHeader = {
  attach() {
    const video = document.getElementById('heroVideo');
    const btn = document.querySelector('.pause-play-btn');

    function pausePlayVideo(e) {
      e.preventDefault();
      if (video.paused) {
        video.play();
      } else {
        video.pause();
      }
    }

    btn.onclick = pausePlayVideo;
  },
};
