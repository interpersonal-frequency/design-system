import heroHeader from './hero-header.twig';
import './hero-header';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Page Headers',
  argTypes: {
    heading: { control: 'text' },
    subheading: { control: 'text' },
    link: { control: 'text' },
    display_video: { control: 'boolean' },
    video: { control: 'text' },
    image: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return heroHeader({ label, ...args });
};

export const headerHero = Template.bind({});
headerHero.args = {
  heading: 'Hero header',
  subheading:
    'Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  image: 'https://via.placeholder.com/1300x500',
  display_video: false,
  video: 'https://www.w3schools.com/howto/rain.mp4',
  link: '/',
};
