import heroIconLinksTemplate from './hero-icon-links.twig';
import heroIconLinksData from './hero-icon-links.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Page Headers',
  argTypes: {
    image: { control: 'text' },
    icon_links: {
      control: { type: 'array' },
    },
  },
};

const Template = ({ label, ...args }) => {
  return heroIconLinksTemplate({ label, ...args });
};

export const heroIconLinks = Template.bind({});
heroIconLinks.args = {
  image: 'https://via.placeholder.com/1300x500',
  icon_links: heroIconLinksData.items,
};
