import searchHeaderTemplate from './search-header.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Page Headers/Search Header',
};

export const searchHeader = () => searchHeaderTemplate();
