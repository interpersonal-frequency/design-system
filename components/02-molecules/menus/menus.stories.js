import breadcrumb from './breadcrumbs/breadcrumbs.twig';
import buttonGroupMenu from './button-group/button-group.twig';
import inlineMenu from './inline/inline-menu.twig';
import mainMenu from './main-menu/main-menu.twig';
import sidebarMenu from './sidebar-menu/sidebar-menu.twig';

import breadcrumbsData from './breadcrumbs/breadcrumbs.yml';
import buttonGroupData from './button-group/button-group.yml';
import inlineMenuData from './inline/inline-menu.yml';
import mainMenuData from './main-menu/main-menu.yml';
import sidebarMenuData from './sidebar-menu/sidebar-menu.yml';

import './main-menu/main-menu';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Menus' };

export const breadcrumbs = () => breadcrumb(breadcrumbsData);

export const buttonGroup = () => buttonGroupMenu(buttonGroupData);

export const inline = () => inlineMenu(inlineMenuData);

export const main = () => mainMenu(mainMenuData);

export const sidebar = () => sidebarMenu(sidebarMenuData);
