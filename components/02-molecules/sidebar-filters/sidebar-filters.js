(function sidebarFilters() {
    Drupal.behaviors.sidebarFilters = {
      attach() {
        const label = document.querySelector('.calendar-label');
  
        if (!document.contains(label)) {
          const eventDatepicker = document.querySelectorAll(
            '[id^="edit-field-start-date-value"]',
          );
          const newsDatepicker = document.querySelectorAll(
            '[id^="edit-field-date-value"]',
          );
          if (eventDatepicker.length > 0) {                        
            Array.from(eventDatepicker).forEach((item) => {
              const calendarLabel = document.createElement('label');
              calendarLabel.setAttribute(
                'class',
                'visually-hidden calendar-label',
              );
              calendarLabel.innerHTML = 'Calendar';
              const datepickerID = item.getAttribute('id');
              calendarLabel.setAttribute('for', datepickerID);
              item.insertAdjacentElement('beforebegin', calendarLabel);
            });
            const eventDatepickerLabel = document.querySelectorAll(
              '[for^="edit-field-start-date-value"]',
            );
            /* eslint-disable */
            Array.from(eventDatepickerLabel).forEach((x) => {
              x.innerHTML = 'Enter date';
            });
            /* eslint-enable */
          } else if (newsDatepicker.length > 0) {            
            Array.from(newsDatepicker).forEach((item) => {
              const calendarLabel = document.createElement('label');
              calendarLabel.setAttribute(
                'class',
                'visually-hidden calendar-label',
              );
              calendarLabel.innerHTML = 'Calendar';
              const datepickerID = item.getAttribute('id');
              calendarLabel.setAttribute('for', datepickerID);
              item.insertAdjacentElement('beforebegin', calendarLabel);
            });
            const newsDatepickerLabel = document.querySelectorAll(
              '[for^="edit-field-date-value"]',
            );
            /* eslint-disable */
            Array.from(newsDatepickerLabel).forEach((y) => {
              y.innerHTML = 'Enter date';
            });
            /* eslint-enable */
          }
        }
      },
    };
  })(Drupal);
  