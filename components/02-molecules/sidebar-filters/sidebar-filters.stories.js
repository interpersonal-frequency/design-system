import sidebarTemplate from './sidebar-filters.twig';
import '../accordions/accordions';
import selectData from '../../01-atoms/forms/select/select.yml';
import './sidebar-filters';

/**
 * Storybook Definition.
 */

export default {
  title: 'Molecules',
  argTypes: {
    heading: { control: 'text' },
    filters: { control: 'object' },
    button_value: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return sidebarTemplate({ label, ...args });
};

export const sidebarFilters = Template.bind({});
sidebarFilters.args = {
  heading: 'Filter Articles',
  button_value: 'Apply Filters',
  filters: { selectData },
};
