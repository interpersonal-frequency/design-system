import bannerTemplate from './simple-banner.twig';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules',
  argTypes: {
    copy: { control: 'text' },
  },
};

const Template = ({ label, ...args }) => {
  return bannerTemplate({ label, ...args });
};

export const simpleBanner = Template.bind({});
simpleBanner.args = {
  copy: 'All locations are open 9:00 a.m.-9:00 p.m. Curbside Service is available at each branch.',
};
