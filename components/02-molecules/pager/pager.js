(function pager() {
  Drupal.behaviors.pager = {
    attach() {
      const item = document.getElementsByClassName('pager__item');

      Array.from(item).forEach((pagerItem) => {
        pagerItem.addEventListener('click', function toggleActive() {
          Array.from(item).forEach((button) => {
            button.classList.remove('is-active');
          });

          this.classList.add('is-active');
        });
      });
    },
  };
})(Drupal);
