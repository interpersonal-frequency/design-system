import leaflet from 'leaflet';

// eslint-disable-next-line func-names
(function ($, Drupal, drupalSettings) {
  'use strict';

  // eslint-disable-next-line no-param-reassign
  Drupal.behaviors.location_map = {
    // eslint-disable-next-line func-names,no-unused-vars
    attach: function attach(context, settings) {
      if (context !== document) {
        return;
      }

      const center = [
        drupalSettings.location_info.map[0].coordinates.lat,
        drupalSettings.location_info.map[0].coordinates.lon,
      ];

      const markerIcon = leaflet.icon({
        iconUrl: `/themes/custom/if_design_system/images/marker-icon.png`,
        iconSize: [31, 46], // size of the icon
        iconAnchor: [15.5, 42], // point of the icon which will correspond to marker's location
        popupAnchor: [0, -45], // point from which the popup should open relative to the iconAnchor
      });

      const map = leaflet
        .map(drupalSettings.location_info.map_id, { scrollWheelZoom: false })
        .setView(center, 17);

      leaflet
        .tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
          attribution:
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        })
        .addTo(map);

      drupalSettings.location_info.map.forEach((point) => {
        leaflet
          .marker([point.coordinates.lat, point.coordinates.lon], {
            icon: markerIcon,
          })
          .addTo(map)
          // eslint-disable-next-line security/detect-non-literal-fs-filename
          .bindPopup(point.heading + point.link);
      });
    },
  };
  // eslint-disable-next-line no-undef
})(jQuery, Drupal, drupalSettings);
