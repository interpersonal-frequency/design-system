import leaflet from 'leaflet';

/**
 * This JS is only for Storybook, not to be used in Drupal.
 */
Drupal.behaviors.location_map = {
  attach() {
    // Initialize the map on the "map" div with a given center and zoom.
    const map = leaflet.map('map').setView([25.74922, -80.26347], 13);
    const markerIcon = leaflet.icon({
      iconUrl: `./marker-icon.png`,
      iconSize: [31, 46], // size of the icon
      iconAnchor: [15.5, 42], // point of the icon which will correspond to marker's location
      popupAnchor: [0, -45], // point from which the popup should open relative to the iconAnchor
    });

    leaflet
      .tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
        attribution:
          '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      })
      .addTo(map);

    leaflet
      .marker([25.74922, -80.26347], {
        icon: markerIcon,
      })
      .addTo(map)
      .bindPopup(
        '<h4>Coral Gables City Hall</h4><a href="https://goo.gl/maps/BQyVdv98e3jzACdH6" target="_blank">405 Biltmore Way,<br /> Coral Gables, FL. 33134</a>',
      );

    leaflet
      .marker([25.74359, -80.25927], {
        icon: markerIcon,
      })
      .addTo(map)
      .bindPopup(
        '<h4>District Court</h4><a href="https://goo.gl/maps/9tmZ8F7b4gYLj9GKA" target="_blank">3100 Ponce de Leon,<br /> Coral Gables, FL. 33134</a>',
      );
  },
};
