import locationMapTemplate from './location-map.twig';
import socialMenu from '../social-links/social-links.yml';
import addresses from './addresses.yml';
import './location-map-example';

/**
 * Storybook Definition.
 */
export default {
  title: 'Molecules/Location Map',
  argTypes: {
    heading: { control: 'text' },
    phone: { control: 'text' },
    email: { control: 'text' },
    social_links: { control: 'object' },
    addresses: { control: 'object' },
    mail_address: { control: 'object' },
    show_social: { control: 'boolean' },
  },
};

const Template = ({ label, ...args }) => {
  return locationMapTemplate({ label, ...args, ...socialMenu });
};

export const locationMap = Template.bind({});
locationMap.args = {
  heading: 'H3 Department Title',
  phone: '(000) 000 0000',
  email: 'name.lastname@county.org',
  addresses: addresses.addresses_items,
  show_social: true,
  mail_address: {
    street_address: 'P.O Box 20,000',
    street_address_2: 'Apartment 2',
    city: 'Grand Junction',
    state: 'CO',
    zip_code: '81502-5001',
  },
};
