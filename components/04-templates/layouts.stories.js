import fullWidthTwig from './full-width.twig';
import withSidebarTwig from './with-sidebar.twig';
import defaultTwig from './_default.twig';

import mainMenu from '../02-molecules/menus/main-menu/main-menu.yml';
import socialMenu from '../02-molecules/social-links/social-links.yml';
import footerData from '../03-organisms/site/site-footer/site-footer.yml';

/**
 * Storybook Definition.
 */
export default {
  title: 'Templates/Layouts',
  parameters: {
    layout: 'fullscreen',
  },
};

export const defaultLayout = () =>
  defaultTwig({ ...mainMenu, ...footerData, ...socialMenu });

export const fullWidth = () =>
  fullWidthTwig({ ...mainMenu, ...footerData, ...socialMenu });

export const withSidebar = () =>
  withSidebarTwig({ ...mainMenu, ...footerData, ...socialMenu });
