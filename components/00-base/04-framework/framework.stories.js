import flexFramework from './flex-framework.twig';
import gridFramework from './grid-framework.twig';

import flexFrameworkData from './flex-framework.yml';
import gridFrameworkData from './grid-framework.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Base/Grid Frameworks' };

export const FlexGridFramework = () => flexFramework(flexFrameworkData);

export const GridLayoutFramework = () => gridFramework(gridFrameworkData);
