## IF Design System

IF Design System is the default Emulsify system for Interpersonal Frequency Drupal projects.

## Development (Lando)

1. Create a `.lando.local.env` file in the root of your local repository
2. Add a line to that file which looks like `FONTAWESOME_NPM_AUTH_TOKEN=TOKEN-GOES-HERE`
3. Replace "TOKEN-GOES-HERE" in that file with a Fontawesome Package manager token
4. Run `lando start`

That should spin up a local server at `http://if-design-system.lndo.site` that you can open in your browser to see the components in a Storybook instance.  It will also watch for updates to your code, so any changes you make will be reflected in the Storybook instance live, any time you save a file.

By default, the lando will be running `npm run develop` in the background so that Emulsify is available at the `lndo.site` domain (above).
This is convenient, but if a change being worked on fails to build, there is no output anywhere for the developer to see.

To get around this issue, a developer can run `lando npm run develop` after the environment has spun up to see the output and any errors that happen during build.
Another IP-based URL will appear that Storybook will also be available to view at.

Output from manually running `lando npm run develop`:
```
ℹ ｢wdm｣: Compiled successfully.
webpack built preview be1fdf059bad193265d1 in 7381ms
╭───────────────────────────────────────────────────╮
│                                                   │
│   Storybook 6.5.3 for Html started                │
│   7.42 s for manager and 7.54 s for preview       │
│                                                   │
│    Local:            http://localhost:6007/       │
│    On your network:  http://192.168.80.2:6007/    │
│                                                   │
╰───────────────────────────────────────────────────╯

```

- Run `lando npm` for any NPM commands

> Examples
> `lando npm run lint`

- Run `lando stop` to turn off the environment
- Run `lando poweroff` to ensure Lando is not running

## Development (no Lando)

- Ensure you're using the correct node/npm version `nvm use`
- npm config set "@fortawesome:registry" https://npm.fontawesome.com/
- npm config set "//npm.fontawesome.com/:_authToken" _Fontawesome-package-manager-token-goes-here_
- Install dependencies `npm install`
- Run the develop script `npm run develop`

That should spin up a local server (typically at `http://localhost:6006`) that you can open in your browser, to see the 
components in a Storybook instance. It will also watch for updates to your code, so any changes you make will be 
reflected in the Storybook instance live, any time you save.